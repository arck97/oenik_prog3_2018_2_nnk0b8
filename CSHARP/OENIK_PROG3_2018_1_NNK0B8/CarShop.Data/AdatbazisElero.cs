﻿// <copyright file="AdatbazisElero.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Az osszes adatbázis elérése egy osztalybol
    /// </summary>
    public static class AdatbazisElero
    {
        /// <summary>
        /// datebaseentity implementálva
        /// </summary>
        private static readonly CarShopDatabasedEntities CarshopAllomanyErtek = new CarShopDatabasedEntities();

        /// <summary>
        /// Gets visszaadja a databaseentitit
        /// </summary>
        public static CarShopDatabasedEntities CarshopAllomany
        {
            get
            {
                return CarshopAllomanyErtek;
            }
        }

        /// <summary>
        /// Gets Tabla neveit visszaadja
        /// </summary>
        public static List<string> Tablanevek
        {
            get
            {
                return CarshopAllomanyErtek.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }

        /// <summary>
        /// Gets Lsitává alakítja az autómákákat
        /// </summary>
        /// <returns>List of automarka</returns>
        public static List<Automarka> Automarkak
        {
            get
            {
                var query = CarshopAllomanyErtek.Automarka.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets modellextrakapcsolo to list
        /// </summary>
        /// <returns>Lost of modellekExtraKapcsolo</returns>
        public static List<ModellExtraKapcsolo> ModellekExtraKapcsolo
        {
            get
            {
                var query = CarshopAllomanyErtek.ModellExtraKapcsolo.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets extrak to list
        /// </summary>
        /// <returns>Lost of extrak</returns>
        public static List<Extrak> Extrak
        {
            get
            {
                var query = CarshopAllomanyErtek.Extrak.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        /// Gets modellek to list
        /// </summary>
        /// <returns>Lost of modellek</returns>
        public static List<Modellek> Modellek
        {
            get
            {
                var query = CarshopAllomanyErtek.Modellek.Select(x => x);
                return query.ToList();
            }
        }

        /// <summary>
        ///  Gets uj elem hozzadasa a markahoz
        /// </summary>
        /// <param name="am">represent the new automarka element</param>
        public static void AddAutomarka(Automarka am)
        {
           CarshopAllomanyErtek.Automarka.Add(am);
        }

        /// <summary>
        ///  Gets uj elem hozzadasa a Extrahoz
        /// </summary>
        /// <param name="e">represent the new extra element</param>
        public static void AddExtra(Extrak e)
        {
            CarshopAllomanyErtek.Extrak.Add(e);
        }

        /// <summary>
        ///  Gets uj elem hozzadasa a tablahoz
        /// </summary>
        /// <param name="m">represent the new modell element</param>
        public static void AddMarka(Modellek m)
        {
            CarshopAllomanyErtek.Modellek.Add(m);
        }

        /// <summary>
        /// uj elemhozzadasa a tablahoz
        /// </summary>
        /// <param name="mek">represent the new modellExtraKapcsolo element</param>
        public static void AddExtraKapcsolo(ModellExtraKapcsolo mek)
        {
            CarshopAllomanyErtek.ModellExtraKapcsolo.Add(mek);
        }

        /// <summary>
        /// elem torlese a tablabol
        /// </summary>
        /// <param name="am">represent the elemtn we want to delete from automarka table</param>
        public static void DeleteAutomarka(Automarka am)
        {
            CarshopAllomanyErtek.Automarka.Remove(am);
        }

        /// <summary>
        /// elem torlese a tablabol
        /// </summary>
        /// <param name="e">represent the elemtn we want to delete from extrak table</param>
        public static void DeleteExtra(Extrak e)
        {
            CarshopAllomanyErtek.Extrak.Remove(e);
        }

        /// <summary>
        /// elem torlese a tablabol
        /// </summary>
        /// <param name="m">represent the elemtn we want to delete from modellek table</param>
        public static void DeleteModell(Modellek m)
        {
            CarshopAllomanyErtek.Modellek.Remove(m);
        }

        /// <summary>
        /// elem torlese a tablabol
        /// </summary>
        /// <param name="mek">represent the elemtn we want to delete from modellekExtraKapcsolo table</param>
        public static void DeleteExtraKapcsolo(ModellExtraKapcsolo mek)
        {
            CarshopAllomanyErtek.ModellExtraKapcsolo.Remove(mek);
        }

        /// <summary>
        /// Gets Elemnti a változásokat
        /// </summary>
        public static void SaveChange()
        {
            CarshopAllomanyErtek.SaveChanges();
        }
    }
}
