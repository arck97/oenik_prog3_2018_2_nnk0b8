//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    /// <summary>
    /// automarka osztaly automata generealas
    /// </summary>
    public partial class Automarka
    {
        /// <summary>
        /// cotr
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Automarka()
        {
            this.Modellek = new HashSet<Modellek>();
        }

    /// <summary>
    /// marka id
    /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// marka nev
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// marka made in 
        /// </summary>
        public string OrszagNev { get; set; }

        /// <summary>
        /// marka alapitasa
        /// </summary>
        public Nullable<System.DateTime> AlapitasEve { get; set; }

        /// <summary>
        /// marka evesfrogalma
        /// </summary>
        public Nullable<int> EvesForgalom { get; set; }
    
        /// <summary>
        /// modellek collekcioja
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Modellek> Modellek { get; set; }
    }
}
