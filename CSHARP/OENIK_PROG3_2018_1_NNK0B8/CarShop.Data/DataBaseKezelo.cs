﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Data
{
    public static class DateBaseKezelo
    {
          private static CarshopDataBasedEntities1 carshopEntities;

        static DateBaseKezelo()
        {
            carshopEntities = new CarshopDataBasedEntities1();
        }

        public static CarshopDataBasedEntities1 CarShopDataBase
        {
            get
            {
                return carshopEntities;
            }
        }

        public static List<Automarka> Osszesautomarka()
        {
            var lekerdezes = carshopEntities.Automarka.Select(x => x);
            return lekerdezes.ToList();
        }

        public static List<Extrak> OsszesExtra()
        {
            var lekerdezes = carshopEntities.Extrak.Select(x => x);
            return lekerdezes.ToList();
        }

        public static List<Modellek> OsszesModell()
        {
            var lekerdezes = carshopEntities.Modellek.Select(x => x);
            return lekerdezes.ToList();
        }

        public static List<string> SQLParancsfutatasaDBn(string query)
        {
            return carshopEntities.Database.SqlQuery<string>(query).ToList();
        }

        public static void SQLParancsFutatasa(string query)
        {
            carshopEntities.Database.ExecuteSqlCommand(query);
        }

        public static List<string> Tablanevek
        {
            get
            {
                return carshopEntities.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }
    }
}
