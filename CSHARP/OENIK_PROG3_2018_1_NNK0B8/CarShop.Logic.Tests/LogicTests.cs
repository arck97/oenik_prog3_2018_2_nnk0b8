﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic test osztaly
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        /// <summary>
        /// beszuras teszteles
        /// </summary>
        [Test]
        public void EgyszeruMarkaInsertCheck()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Automarka> lista = new List<Automarka>() { new Automarka() { Id = 666666, AlapitasEve = DateTime.Parse("1997-01-01"), EvesForgalom = 50, Nev = "asd", OrszagNev = "ads" } };

            m2.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());
            m2.Object.HozzaadAutomarka("asd", "ads", DateTime.Parse("1997-01-01"), 20000);
            m2.Verify(x => x.HozzaadAutomarka("asd", "ads", DateTime.Parse("1997-01-01"), 20000), Times.Once());
        }

        /// <summary>
        /// beszuras modell teszteles
        /// </summary>
        [Test]
        public void EgyszeruModelInsertCheck()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Modellek> lista = new List<Modellek>() { new Modellek() { MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), MotorTerfogat = 1400, Loero = 60, AlapAr = 150000, Nev = "asd" } };

            m2.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista.AsQueryable());
            m2.Object.HozzaadModellek(1, DateTime.Parse("2000-02-02"), 1400, 60, 150000, "asd");
            m2.Verify(x => x.HozzaadModellek(1, DateTime.Parse("2000-02-02"), 1400, 60, 150000, "asd"), Times.Once());
        }

        /// <summary>
        /// extrainsertteszt
        /// </summary>
        [Test]
        public void EgyszeruExtraInsertCheck()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Extrak> lista = new List<Extrak>() { new Extrak() { KategoriaNev = "plázacica", Nev = "ruhaadagolo", Ar = 400000, Szin = "töbszinü", TobszorHasznalhatoE = true } };

            m2.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista.AsQueryable());
            m2.Object.HozzaadExtrak("plázacica", "ruhaadagolo", 400000, "töbszinü", true);
            m2.Verify(x => x.HozzaadExtrak("plázacica", "ruhaadagolo", 400000, "töbszinü", true), Times.Once());
        }

        /// <summary>
        /// kapcsoloinsert teszt
        /// </summary>
        [Test]
        public void EgyszeruKapcsoloInsertCheck()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<ModellExtraKapcsolo> lista = new List<ModellExtraKapcsolo>() { new ModellExtraKapcsolo() { ModellID = 32323, ExtrakID = 232323 } };

            m2.Setup(x => x.Egyesitettrepo.Mekrepo.All).Returns(lista.AsQueryable());
            m2.Object.HozzadKapcsolo(32323, 232323);
            m2.Verify(x => x.HozzadKapcsolo(32323, 232323), Times.Once());
        }

        /// <summary>
        /// updatemodelteszt
        /// </summary>
        [Test]
        public void UpdateModelCheck()
        {
            BusinessLogic bl = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(bl);
            List<Modellek> lista = new List<Modellek>();
            lista.Add(new Modellek() { MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), MotorTerfogat = 1400, Loero = 60, AlapAr = 150000, Nev = "asd" });

            m2.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.FirissitModellek(1, 2, DateTime.Parse("2000-02-02"), 1400, 60, 15000, "asd"), Is.False);
        }

        /// <summary>
        /// marka
        /// </summary>
        /// <param name="value"> marak id-ja</param>
        [Test]
        [Sequential]
        public void UpdateMarkaekCheck([Values(100)] int value)
        {
            BusinessLogic bl = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(bl);
            List<Automarka> lista = new List<Automarka>();
            lista.Add(new Automarka() { Id = 666666, AlapitasEve = DateTime.Parse("1997-01-01"), EvesForgalom = 50, Nev = "asd", OrszagNev = "ads" });

            m2.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.FirissitAutomarka(value, "asd", "ads", DateTime.Parse("1997-01-01"), 50), Is.False);
        }

        /// <summary>
        /// UpdateExtra
        /// </summary>
        /// <param name="value"> Extra id-ja</param>
        [Test]
        [Sequential]
        public void UpdateExtra([Values(100)] int value)
        {
            BusinessLogic bl = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(bl);
            List<Extrak> lista = new List<Extrak>();
            lista.Add(new Extrak() { Id = 1, KategoriaNev = "blazacica", Nev = "ruha", Ar = 21212, Szin = "sárga", TobszorHasznalhatoE = true });

            m2.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.FirissitExtra(value, "blazacica", "ruha", 21212, "sárga", true), Is.False);
        }

        /// <summary>
        /// UpdateKapcsolo
        /// </summary>
        /// <param name="value"> Kapcsolo id-ja</param>
        [Test]
        [Sequential]
        public void UpdateKapcsolo([Values(100)] int value)
        {
            BusinessLogic bl = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(bl);
            List<Automarka> lista = new List<Automarka>();
            lista.Add(new Automarka() { Id = 666666, AlapitasEve = DateTime.Parse("1997-01-01"), EvesForgalom = 50, Nev = "asd", OrszagNev = "ads" });

            m2.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.FirissitAutomarka(value, "asd", "ads", DateTime.Parse("1997-01-01"), 50), Is.False);
        }

        /// <summary>
        /// A simple car deletion check.
        /// </summary>
        [Test]
        public void ModelDeletionCheck()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Modellek> lista = new List<Modellek>();

            lista.Add(new Modellek() { Id = 1, MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), MotorTerfogat = 300, Nev = "asd" });

            m2.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.RemoveModellek(1), Is.False);
        }

        /// <summary>
        /// markadelation
        /// </summary>
        [Test]
        public void MarkaDeletatoin()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Automarka> lista = new List<Automarka>();

            lista.Add(new Automarka() { Id = 1, Nev = "asd", OrszagNev = "sad", AlapitasEve = DateTime.Parse("2000-02-02"), EvesForgalom = 30000 });

            m2.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.RemoveAutomarka(1), Is.False);
        }

        /// <summary>
        /// extradelation
        /// </summary>
        [Test]
        public void ExtraDelation()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<Extrak> lista = new List<Extrak>();

            lista.Add(new Extrak() { Id = 1, KategoriaNev = "plázacica", Nev = "ruhas", Ar = 20000, Szin = "több", TobszorHasznalhatoE = true });

            m2.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.RemoveExtra(1), Is.False);
        }

        /// <summary>
        /// kapcsolotorles
        /// </summary>
        [Test]
        public void KapcsoloDelation()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> m2 = Mock.Get(m);
            List<ModellExtraKapcsolo> lista = new List<ModellExtraKapcsolo>();

            lista.Add(new ModellExtraKapcsolo() { Id = 1, ModellID = 2, ExtrakID = 3 });

            m2.Setup(x => x.Egyesitettrepo.Mekrepo.All).Returns(lista.AsQueryable());

            Assert.That(m2.Object.RemoveKapcsolo(1), Is.False);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void AutomarkaOlvas()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Automarka> lista = new List<Automarka>() { new Automarka() { AlapitasEve = DateTime.Parse("2000-02-02"), EvesForgalom = 50, OrszagNev = "das", Nev = "asd" } };
            logicMock.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());
            List<Automarka> list2 = logicMock.Object.Egyesitettrepo.Autorepo.All.ToList();
            Assert.That(list2.Count >= 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ModelekOlvas()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Modellek> lista = new List<Modellek>() { new Modellek() { MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), AlapAr = 250, Loero = 120, MotorTerfogat = 120, Nev = "asd" } };

            logicMock.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista.AsQueryable());
            List<Modellek> list2 = logicMock.Object.Egyesitettrepo.Modelrepo.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void ExtraOlvas()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Extrak> lista = new List<Extrak>() { new Extrak() { KategoriaNev = "asd", Nev = "ads", Ar = 0, Szin = "szörke", TobszorHasznalhatoE = true } };

            logicMock.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista.AsQueryable());
            List<Extrak> list2 = logicMock.Object.Egyesitettrepo.Extrarepo.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void KapcsoloOlvas()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<ModellExtraKapcsolo> lista = new List<ModellExtraKapcsolo>() { new ModellExtraKapcsolo { ModellID = 1, ExtrakID = 1 } };

            logicMock.Setup(x => x.Egyesitettrepo.Mekrepo.All).Returns(lista.AsQueryable());
            List<ModellExtraKapcsolo> list2 = logicMock.Object.Egyesitettrepo.Mekrepo.All.ToList();
            Assert.That(list2.Count > 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void Kocsiletezese()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Automarka> lista = new List<Automarka>() { new Automarka() { AlapitasEve = DateTime.Parse("2000-02-02"), EvesForgalom = 50, OrszagNev = "asd", Nev = "ads" } };
            logicMock.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());
            bool letezik = logicMock.Object.LetezikAutomarka(1);
            Assert.That(letezik == false);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void Modellletezese()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Modellek> lista2 = new List<Modellek>() { new Modellek() { MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), AlapAr = 250000, Loero = 120, MotorTerfogat = 12220, Nev = "Jani" } };
            logicMock.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista2.AsQueryable());
            bool letezik = logicMock.Object.LetezikModell(1);
            Assert.That(letezik == false);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void Extraletezese()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Extrak> lista3 = new List<Extrak>()
            {
                new Extrak()
            {
                Id = 1, KategoriaNev = "ad", Nev = "asd", Ar
                = 0, Szin = "black", TobszorHasznalhatoE = true
            }
            };
            logicMock.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista3.AsQueryable());
            bool letezik = logicMock.Object.LetezikExtra(1);
            Assert.That(letezik == true);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void AutoKeresNevszerint()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logicMock = Mock.Get(m);
            List<Automarka> lista = new List<Automarka>() { new Automarka() { Id = 1, AlapitasEve = DateTime.Parse("2000-02-02"), EvesForgalom = 5000, OrszagNev = "asd", Nev = "ads" } };
            logicMock.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista.AsQueryable());
            int id = logicMock.Object.AutomarkaKereses("asd");
            Assert.That(id, Is.EqualTo(0));
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void GetatlagSzin()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logic = Mock.Get(m);
            List<Extrak> lista3 = new List<Extrak>()
            {
                new Extrak()
            {
                Id = 1, KategoriaNev = "ad", Nev = "asd", Ar
                = 0, Szin = "black", TobszorHasznalhatoE = true
            }
            };
            logic.Setup(x => x.Egyesitettrepo.Extrarepo.All).Returns(lista3.AsQueryable());
            double letezik = logic.Object.GetAtlagExtraSzin("sárga");
            Assert.That(letezik == 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void GetatlagModell()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logic = Mock.Get(m);
            List<Modellek> lista3 = new List<Modellek>()
            {
                new Modellek()
            {
                MarkaID = 1, MegjelenesNapja = DateTime.Parse("2000-02-02"), AlapAr = 250000, Loero = 120, MotorTerfogat = 12220, Nev = "Jani"
            }
            };
            logic.Setup(x => x.Egyesitettrepo.Modelrepo.All).Returns(lista3.AsQueryable());
            double letezik = logic.Object.GetAtlagModel(2);
            Assert.That(letezik == 0);
        }

        /// <summary>
        /// test
        /// </summary>
        [Test]
        public void GetatlagMarka()
        {
            BusinessLogic m = Mock.Of<BusinessLogic>();
            Mock<BusinessLogic> logic = Mock.Get(m);
            List<Automarka> lista6 = new List<Automarka>()
            {
                new Automarka()
            {
                Id = 1, AlapitasEve = DateTime.Parse("2000-02-02"), EvesForgalom = 5000, OrszagNev = "asd", Nev = "ads"
            }
            };
            logic.Setup(x => x.Egyesitettrepo.Autorepo.All).Returns(lista6.AsQueryable());
            double letezik = logic.Object.GetAtlagMarkak("USA");
            Assert.That(letezik == 0);
        }
    }
}
