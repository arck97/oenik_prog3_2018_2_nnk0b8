﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// üzleti logika megvalositasa
    /// </summary>
    public class BusinessLogic : ILogic
    {
        /// <summary>
        /// egyesítettrepo példánya
        /// </summary>
        private readonly EgyesitettRepository egyesitettrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        public BusinessLogic()
        {
            this.egyesitettrepo = new EgyesitettRepository();
        }

        /// <summary>
        /// Gets tablatartalmanak visszaadasa
        /// </summary>
        public StringBuilder AutomarkaTartalom => this.Egyesitettrepo.Autorepo.TablaTartalma;

        /// <summary>
        /// Gets tablatartalmanak visszaadasa
        /// </summary>
        public StringBuilder ExtraTartalom => this.Egyesitettrepo.Extrarepo.TablaTartalma;

        /// <summary>
        /// Gets tablatartalmanak visszaadasa
        /// </summary>
        public StringBuilder ModellTartalom => this.Egyesitettrepo.Modelrepo.TablaTartalma;

        /// <summary>
        /// Gets tablatartalmanak visszaadasa
        /// </summary>
        public StringBuilder Kapcsolotartalom => this.Egyesitettrepo.Mekrepo.TablaTartalma;

        /// <summary>
        /// Gets összes tabla nevet adja vissza
        /// </summary>
        public List<string> OsszesTabla => this.Egyesitettrepo.Tablanevek();

        /// <summary>
        /// Gets egyesitettrepo property
        /// </summary>
        public virtual EgyesitettRepository Egyesitettrepo => this.egyesitettrepo;

        /// <summary>
        /// kereses nev alapjan
        /// </summary>
        /// <param name="marka">marka alapjan keres</param>
        /// <returns>markaval tér vissza</returns>
        public virtual int AutomarkaKereses(string marka)
        {
            return this.egyesitettrepo.Autorepo.All.Where(x => x.Nev == marka).Select(x => x.Id).FirstOrDefault();
        }

        /// <summary>
        /// tabla frisitese
        /// </summary>
        /// <param name="id">marka ID</param>
        /// <param name="nev">marka nev</param>
        /// <param name="orszagnev">made in</param>
        /// <param name="alapitaseve">alapitasi datum</param>
        /// <param name="evesforgalom">marka evesforglma</param>
        /// <returns>true ha igaz</returns>
        public virtual bool FirissitAutomarka(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                return this.Egyesitettrepo.Autorepo.Frissit(id, nev, orszagnev, alapitaseve, evesforgalom);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a frissitésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// tabla frisitese
        /// </summary>
        /// <param name="id">exra ID</param>
        /// <param name="kategorianev">extra kategorianak a neve</param>
        /// <param name="nev">extra neve</param>
        /// <param name="ar">extra ara</param>
        /// <param name="szin">extra szine</param>
        /// <param name="tobszorhasznalhatoE">extra hasznalhatosaga</param>
        /// <returns>true ha igaz</returns>
        public virtual bool FirissitExtra(int id, string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE)
        {
            try
            {
                return this.Egyesitettrepo.Extrarepo.Frissit(id, kategorianev, nev, ar, szin, tobszorhasznalhatoE);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a frissitésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// tabla frisitese
        /// </summary>
        /// <param name="id">model id</param>
        /// <param name="markaID">model kapcsolo marka id</param>
        /// <param name="megjelenesnapja">model megjelenese</param>
        /// <param name="motorterfogat">modell motorteerfogat</param>
        /// <param name="loero">motol loero</param>
        /// <param name="alapar">model alapar </param>
        /// <param name="nev">model neve</param>
        /// <returns>true ha igaz</returns>
        public virtual bool FirissitModellek(int id, int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev)
        {
            try
            {
                return this.Egyesitettrepo.Modelrepo.Frissit(id, markaID, megjelenesnapja, motorterfogat, loero, alapar, nev);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a frissitésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// tabla frisitese
        /// </summary>
        /// <param name="id">kapcsolo id</param>
        /// <param name="extraid">kapcsolo tabla extra id kapcsoloja</param>
        /// <param name="modellid">kapcsolo tabla model id kaocsoloja</param>
        /// <returns>true ha igaz</returns>
        public virtual bool FirssitKapcsolo(int id, int extraid, int modellid)
        {
            try
            {
                return this.Egyesitettrepo.Mekrepo.Frissit(id, extraid, modellid);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a frissitésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem hozzadasa
        /// </summary>
        /// <param name="nev">marak neve</param>
        /// <param name="orszagnev">made in</param>
        /// <param name="alapitaseve">marka alapitasa</param>
        /// <param name="evesforgalom">marka evesforgalma</param>
        /// <returns>true ha igaz</returns>
        public virtual bool HozzaadAutomarka(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                return this.Egyesitettrepo.Autorepo.HozzaAd(nev, orszagnev, alapitaseve, evesforgalom);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem hozzadasa
        /// </summary>
        /// <param name="kategorianev">extra kategoria neve</param>
        /// <param name="nev">extra neve</param>
        /// <param name="ar">extra ara</param>
        /// <param name="szin">extra szine</param>
        /// <param name="tobszorhasznalhatoE">extra felhasznalhatosaga</param>
        /// <returns>true ha igaz</returns>
        public virtual bool HozzaadExtrak(string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE)
        {
            try
            {
                return this.Egyesitettrepo.Extrarepo.HozzaAd(kategorianev, nev, ar, szin, tobszorhasznalhatoE);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem hozzadasa
        /// </summary>
        /// <param name="markaID">model marka id kapcsolo</param>
        /// <param name="megjelenesnapja">model megjelenese</param>
        /// <param name="motorterfogat">model motorterfogat</param>
        /// <param name="loero">motor loero</param>
        /// <param name="alapar">model alapar</param>
        /// <param name="nev">model neve</param>
        /// <returns>true ha igaz</returns>
        public virtual bool HozzaadModellek(int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev)
        {
            try
            {
                return this.Egyesitettrepo.Modelrepo.HozzaAd(markaID, megjelenesnapja, motorterfogat, loero, alapar, nev);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem hozzadasa
        /// </summary>
        /// <param name="extraid">extra tabla kapcsolo id</param>
        /// <param name="modellid">model tabla kapcsolo id</param>
        /// <returns>true ha igaz</returns>
        public virtual bool HozzadKapcsolo(int extraid, int modellid)
        {
            try
            {
                return this.Egyesitettrepo.Mekrepo.HozzaAd(extraid, modellid);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a hozzáadásnál: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem letenek vizsgalata
        /// </summary>
        /// <param name="id">idju elem letezik-e</param>
        /// <returns>true ha igaz</returns>
        public bool LetezikAutomarka(int id)
        {
            return this.Egyesitettrepo.Autorepo.AutomarkaLetezikE(id);
        }

        /// <summary>
        /// elem letenek vizsgalata
        /// </summary>
        /// <param name="id">idju elem letezik-e</param>
        /// <returns>true ha igaz</returns>
        public bool LetezikExtra(int id)
        {
            return this.Egyesitettrepo.Extrarepo.ExtraletezikE(id);
        }

        /// <summary>
        /// elem letenek vizsgalata
        /// </summary>
        /// <param name="id">idju elem letezik-e</param>
        /// <returns>true ha igaz</returns>
        public bool LetezikModell(int id)
        {
            return this.Egyesitettrepo.Modelrepo.ModelletezikE(id);
        }

        /// <summary>
        /// elem törlese
        /// </summary>
        /// <param name="id">idjuelem torlse</param>
        /// <returns>true ha igaz</returns>
        public virtual bool RemoveAutomarka(int id)
        {
            try
            {
                return this.Egyesitettrepo.Autorepo.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem törlese
        /// </summary>
        /// <param name="id">id-ju elem torlese</param>
        /// <returns>true ha igaz</returns>
        public virtual bool RemoveExtra(int id)
        {
            try
            {
                return this.Egyesitettrepo.Extrarepo.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem törlese
        /// </summary>
        /// <param name="id">id-ju elem torlese</param>
        /// <returns>true ha igaz</returns>
        public virtual bool RemoveKapcsolo(int id)
        {
            try
            {
                return this.Egyesitettrepo.Mekrepo.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// elem törlese
        /// </summary>
        /// <param name="id">id-ju elem torlese</param>
        /// <returns>true ha igaz</returns>
        public virtual bool RemoveModellek(int id)
        {
            try
            {
                return this.Egyesitettrepo.Modelrepo.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Hiba a törlésnél: " + e);
            }

            return false;
        }

        /// <summary>
        /// 2000 elött alapitott markak
        /// </summary>
        /// <returns>adatok listaja stringbe</returns>
        public virtual List<string> CarAfter2000()
        {
            List<string> adatok = new List<string>();
            var q = from x in this.Egyesitettrepo.Autorepo.All
                    join model in this.Egyesitettrepo.Modelrepo.All on x.Id equals model.MarkaID
                    where x.AlapitasEve > DateTime.Parse("2000-01-01")
                    select new { Markaneve = x.Nev, Modellneve = model.Nev, Alapitasidatum = x.AlapitasEve.ToString() };

            foreach (var item in q)
            {
                adatok.Add(item.Markaneve + ", " + item.Modellneve + ", " + item.Alapitasidatum);
            }

            return adatok;
        }

        /// <summary>
        /// amerikai auto neveit lekeri a modellel egyutt
        /// </summary>
        /// <returns>lista a modellel és a markaval</returns>
        public virtual List<string> AmerikaiAutokModell()
        {
            List<string> temp = new List<string>();
            var q = from x in this.Egyesitettrepo.Autorepo.All
                    join modell in this.Egyesitettrepo.Modelrepo.All on x.Id equals modell.MarkaID
                    where x.OrszagNev == "USA"
                    select new { markanev = x.Nev, Modelnev = modell.Nev };

            foreach (var item in q)
            {
                temp.Add(item.markanev + " " + item.Modelnev);
            }

            return temp;
        }

        /// <summary>
        /// alapar magasabbmint marka bevetele
        /// </summary>
        /// <returns>string lista ahol magasabb az alapar </returns>
        public virtual List<string> AlaparMagasabbMintEvesForgalom()
        {
            List<string> tmp = new List<string>();

            var q = from x in this.Egyesitettrepo.Autorepo.All
                    join modell in this.Egyesitettrepo.Modelrepo.All on x.Id equals modell.MarkaID
                    where x.EvesForgalom < modell.AlapAr
                    select new { Markak = x.Nev };

            foreach (var item in q)
            {
                tmp.Add(item.Markak);
            }

            return tmp;
        }

        /// <summary>
        /// Gets average of extras color.
        /// </summary>
        /// <param name="color">Color we look for.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAtlagExtraSzin(string color)
        {
            try
            {
                return this.Egyesitettrepo.Extrarepo.AtlagExtraSzin(color);
            }
            catch (Exception e)
            {
                Console.WriteLine("Átlag számítási hiba " + color + " " + e);
            }

            return -1;
        }

        /// <summary>
        /// Gets average amount of brands by country.
        /// </summary>
        /// <param name="country">Country name.</param>
        /// <returns>Number of brands in country.</returns>
        public virtual double GetAtlagMarkak(string country)
        {
            try
            {
                return this.Egyesitettrepo.Autorepo.AtlagosMarkaOrszagonkent(country);
            }
            catch (Exception e)
            {
                Console.WriteLine("Átlag számítási hiba " + country + " " + e);
            }

            return -1;
        }

        /// <summary>
        /// Gets average amount of brand.
        /// </summary>
        /// <param name="brandid">ID of brand.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAtlagModel(int brandid)
        {
            try
            {
                return this.Egyesitettrepo.Modelrepo.AtlagosModel(brandid);
            }
            catch (Exception e)
            {
                Console.WriteLine("Átlag számítási hiba " + brandid + " " + e);
            }

            return -1;
        }
    }
}