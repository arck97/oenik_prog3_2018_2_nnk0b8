﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// interface amit  alogic osztaly majd megvalosit
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets stringé alakítja a tábla tartalmát
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        StringBuilder AutomarkaTartalom { get; }

        /// <summary>
        /// Gets stringé alakítja a tábla tartalmát
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        StringBuilder ExtraTartalom { get; }

        /// <summary>
        /// Gets stringé alakítja a tábla tartalmát
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        StringBuilder ModellTartalom { get; }

        /// <summary>
        /// Gets stringé alakítja a tábla tartalmát
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        StringBuilder Kapcsolotartalom { get; }

        /// <summary>
        /// Gets összestabla nevet visszaadja
        /// </summary>
        /// <returns> listak a tabla nevekkel</returns>
        List<string> OsszesTabla { get; }

        /// <summary>
        /// modell tablahoz ad új elemet
        /// </summary>
        /// <param name="markaID">azonosito</param>
        /// <param name="megjelenesnapja">modell megjelenesenk napja</param>
        /// <param name="motorterfogat">terfogat</param>
        /// <param name="loero">loero</param>
        /// <param name="alapar">induloar</param>
        /// <param name="nev">modell neve</param>
        /// <returns>sikeres beszurasnal true</returns>
        bool HozzaadModellek(int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev);

        /// <summary>
        /// automarka tablahoz ad új elemet
        /// </summary>
        /// <param name="nev">marka neve</param>
        /// <param name="orszagnev">made in</param>
        /// <param name="alapitaseve">mikor alapitottak a ceget</param>
        /// <param name="evesforgalom">mennyi az eves bevetele</param>
        /// <returns>sikeres beszurasnal true</returns>
        bool HozzaadAutomarka(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom);

        /// <summary>
        /// hozzaad egy új elemet az extrakhoz
        /// </summary>
        /// <param name="kategorianev">kategorianeve</param>
        /// <param name="nev">extra neve</param>
        /// <param name="ar">extra ara</param>
        /// <param name="szin">kocsi szine</param>
        /// <param name="tobszorhasznalhatoE">töbször is hasznalhatom-e</param>
        /// <returns>sikeres beszurasnal true</returns>
        bool HozzaadExtrak(string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE);

        /// <summary>
        /// kapcsolo hozzáadása
        /// </summary>
        /// <param name="extraid">Kapcsolohoz tartozo extra Id-ja </param>
        /// <param name="modellid">Kapcsolohoz tartozo model ID-ja </param>
        /// <returns>true ha sikeres volt</returns>
        bool HozzadKapcsolo(int extraid, int modellid);

        /// <summary>
        /// update-el egy elemet az automarkaba
        /// </summary>
        /// <param name="id">extra azonositoja</param>
        /// <param name="nev">extra neve</param>
        /// <param name="orszagnev">orszagnev</param>
        /// <param name="alapitaseve">alapitaseve</param>
        /// <param name="evesforgalom">eves bevetel</param>
        /// <returns>true ha sikeres volt</returns>
        bool FirissitAutomarka(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom);

        /// <summary>
        /// updatel egy elemet a modellekben
        /// </summary>
        /// <param name="id"> azonosito</param>
        /// <param name="markaID">marka azonosito</param>
        /// <param name="megjelenesnapja">megjelenesnapja</param>
        /// <param name="motorterfogat">térfogat</param>
        /// <param name="loero">loero</param>
        /// <param name="alapar">alapar</param>
        /// <param name="nev">modell neve</param>
        /// <returns>true ha sikeres volt</returns>
        bool FirissitModellek(int id, int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev);

        /// <summary>
        /// updatel egy elemet a extra tablaban
        /// </summary>
        /// <param name="id">extra azonositoja</param>
        /// <param name="kategorianev">kategorianev</param>
        /// <param name="nev">nev</param>
        /// <param name="ar">ára</param>
        /// <param name="szin">szine</param>
        /// <param name="tobszorhasznalhatoE">tobszorhasznalhato-e</param>
        /// <returns>true ha sikeres volt</returns>
        bool FirissitExtra(int id, string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE);

        /// <summary>
        /// kapcsolok tabla firsitese
        /// </summary>
        /// <param name="id">tabla Id</param>
        /// <param name="extraid">extratabla Id kapcsolja</param>
        /// <param name="modellid">model tabla kapcsolo ID </param>
        /// <returns>true ha sikeres</returns>
        bool FirssitKapcsolo(int id, int extraid, int modellid);

        /// <summary>
        ///  ID-ju elem tolrese
        /// </summary>
        /// <param name="id">törlendo elem Id-ja</param>
        /// <returns>True ha sikeres</returns>
        bool RemoveExtra(int id);

        /// <summary>
        ///  ID-ju elem tolrese
        /// </summary>
        /// <param name="id">törlendo elem Id-ja</param>
        /// <returns>True ha sikeres</returns>
        bool RemoveModellek(int id);

        /// <summary>
        /// ID-ju elem tolrese
        /// </summary>
        /// <param name="id">törlendo elem Id-ja</param>
        /// <returns>True ha sikeres</returns>
        bool RemoveAutomarka(int id);

        /// <summary>
        ///  ID-ju elem tolrese
        /// </summary>
        /// <param name="id">törlendo elem Id-ja</param>
        /// <returns>True ha sikeres</returns>
        bool RemoveKapcsolo(int id);

        /// <summary>
        /// Megvizsgálja létezik-e egy iD marka
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// /// <returns>True sikernél</returns>
        bool LetezikAutomarka(int id);

        /// <summary>
        /// Megvizsgálja létezik-e egy iD modell
        /// </summary>
        /// <param name="id">modell id-ja</param>
        /// <returns>True ha létezik iylen modell</returns>
        bool LetezikModell(int id);

        /// <summary>
        /// Megvizsgálja létezik-e egy adott extra
        /// </summary>
        /// <param name="id">extra ID-ja</param>
        /// <returns>True ha létezik az extra</returns>
        bool LetezikExtra(int id);
    }
}
