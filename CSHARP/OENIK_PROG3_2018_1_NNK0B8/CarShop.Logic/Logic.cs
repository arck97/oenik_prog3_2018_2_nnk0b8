﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarShop.Repository;
using CarShop.Data;



namespace CarShop.Logic
{
    public class Logic : ILogic
    {
        AutomarkaRepository autorepo = new AutomarkaRepository();
        ExtraRepository extrarepo = new ExtraRepository();
        ModellRepository modelrepo = new ModellRepository();

        public void ExtrakAzArNemTobbMint50k()
        {
            throw new NotImplementedException();
        }

        public void ExtrakLegolcsobbKozepesKategorias()
        {
            throw new NotImplementedException();
        }

        public void ExtrakVanSzine()
        {
            throw new NotImplementedException();
        }

        public bool FirissitAutomarka(int ID, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                autorepo.Frissit("UPDATE [Automarka] SET Nev= '" + nev + "', OrszagNev= '" + orszagnev + "', AlapitasEve= '"
                    + alapitaseve.ToString("yyyy-MM-dd") + "', EvesForgalom= '" + evesforgalom + "' WHERE Id = '" + ID + "'");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A Frissítés nem siekrült! " + e);
            }
            return false;

        }

        public bool FirissitExtra(int ID, string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE)
        {
            try
            {
                extrarepo.Frissit("UPDATE [Extrak] SET Kategorianev= '" + kategorianev + "', nev= '" + nev + "', ar= '"
                    + ar + "', szin= '" + szin + "', tobszorhasznalhato='" + tobszorhasznalhatoE + "' WHERE Id = '" + ID + "'");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A Frissítés nem siekrült! " + e);
            }
            return false;
        }

        public bool FirissitModellek(int ID, int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev)
        {
            try
            {
                modelrepo.Frissit("UPDATE [Modellek] SET MarkaID= '" + markaID + "', megjelenesnapja= '" + megjelenesnapja.ToString("yyyy-MM-dd") + "', motorterfogat= '"
                    + motorterfogat + "', loerő= '" + loero + "', alapár='" + alapar + "', nev='" + nev + "' WHERE Id = '" + ID + "'");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A Frissítés nem siekrült! " + e);
            }
            return false;
        }

        public bool HozzaadAutomarka( string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            try
            {
                autorepo.Hozzaad("INSERT INTO [Automarka] (ID,nev , orszagnev , alapitaseve , evesforgalom ) VALUES('" + nev + "','" + orszagnev + "','" + alapitaseve.ToString("yyyy-MM-dd") + "','" + evesforgalom + "','");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A hozzáadaás sikertelen " + e);
            }
            return false;
        }

        public bool HozzaadExtrak(string kategorianev, string nev, int ar, string szin, bool tobszorhasznalhatoE)
        {
            try
            {
                extrarepo.Hozzaad("INSERT INTO [Extrak] (kategorianev , nev , ar , szin, tobszorhasználhatoE ) VALUES('" + kategorianev + "','" + nev + "','" + ar + "','" + szin + "','" + tobszorhasznalhatoE + "','");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A hozzáadaás sikertelen " + e);
            }
            return false;
        }

        public bool HozzaadModellek(int markaID, DateTime megjelenesnapja, int motorterfogat, int loero, int alapar, string nev)
        {
            try
            {
                modelrepo.Hozzaad("INSERT INTO [Modellek] (markaID , megjelenésnapja , motortérfogat , loero , alapar , nev ) VALUES('" + markaID + "','" + megjelenesnapja.ToString("yyyy-MM-dd") + "','" + motorterfogat + "','" + loero + "','" + alapar + "','" + nev + "','");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("A hozzáadaás sikertelen " + e);
            }
            return false;
        }

        public void MarkaAlacsonyEvesForgalom()
        {
            throw new NotImplementedException();
        }

        public void MarkaJapánAutokSzama()
        {
            throw new NotImplementedException();
        }

        public void MarkaKoranLettAlapitva()
        {
            throw new NotImplementedException();
        }

        public void ModellekLegjobbTeljesitmeny()
        {
            throw new NotImplementedException();
        }

        public void ModellekLegjobbÉrték_Loero()
        {
            throw new NotImplementedException();
        }

        public void ModellekLegnagyobbLoero()
        {
            throw new NotImplementedException();
        }

        public List<Automarka> OlvasAutomarka()
        {
            return autorepo.Olvas();
        }

        public List<Extrak> OlvasExtrak()
        {
            return extrarepo.Olvas();
        }

        public List<Modellek> OlvasModellek()
        {
            return modelrepo.Olvas();
        }

        public bool TorolAutomarka(int ID)
        {
            try
            {
                autorepo.Torles("DELETE FROM [Automarka] where ID = " + ID);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Sikertelen torles " + ID);
            }
            return false;

        }

        public bool TorolExtra(int ID)
        {
            try
            {
                autorepo.Torles("DELETE FROM [Extrak] where ID = " + ID);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Sikertelen torles " + ID);
            }
            return false;
        }

        public bool TorolModellek(int ID)
        {
            try
            {
                autorepo.Torles("DELETE FROM [Modellek] where ID = " + ID);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Sikertelen torles " + ID);
            }
            return false;
        }
    }
}

