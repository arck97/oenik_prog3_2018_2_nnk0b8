﻿// <copyright file="AutomarkaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Automarka repo letrehozasa
    /// </summary>
    public class AutomarkaRepository : IRepository<Automarka>
    {
        /// <summary>
        /// markak reprezentácioja
        /// </summary>
        private readonly List<Automarka> automarkak;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutomarkaRepository"/> class.
        /// </summary>
        /// <returns>márkák</returns>
        public AutomarkaRepository()
        {
            this.automarkak = AdatbazisElero.Automarkak;
        }

        /// <summary>
        /// Gets automarka content
        /// </summary>
        /// <returns>Tábla tartalma stringként</returns>
        public StringBuilder TablaTartalma
        {
            get
            {
                List<Automarka> temp = this.All.ToList();
                StringBuilder sb = new StringBuilder();
                foreach (var x in temp)
                {
                    sb.AppendLine("-* ID: " + x.Id + " , NÉV: " + x.Nev + " , ORSZÁG: " + x.OrszagNev + " , ÉVES BEVÉTEL: " + x.EvesForgalom + " , ALAPÍTÁS ÉVE: " + x.AlapitasEve);
                }

                return sb;
            }
        }

        /// <summary>
        /// Gets the automarka content
        /// </summary>
        /// <returns>márkák</returns>
        public virtual IQueryable<Automarka> All => this.automarkak.AsQueryable();

        /// <summary>
        /// Egy elem hozzáadása az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public bool HozzaAd(string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            Automarka auto = new Automarka()
            {
                Nev = nev,
                OrszagNev = orszagnev,
                AlapitasEve = alapitaseve,
                EvesForgalom = evesforgalom
            };
            AdatbazisElero.AddAutomarka(auto);
            AdatbazisElero.SaveChange();
            return true;
        }

        /// <summary>
        /// Egy elem törlése az automarka táblából
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Egy adott elem ID-ja</param>
        /// <returns>True sikernél</returns>
        public bool Remove(int id)
        {
            List<Automarka> auto = this.automarkak.Where(t => t.Id == id).Select(x => x).ToList();
            if (auto.Count > 0)
            {
                AdatbazisElero.DeleteAutomarka(auto[0]);
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Egy elem frissítése az automárka táblához
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// <param name="nev">Automarka nev</param>
        /// <param name="orszagnev">Alapítási ország neve</param>
        /// <param name="alapitaseve">Alapítás éve</param>
        /// <param name="evesforgalom">Éves forgalom</param>
        /// <returns>True sikernél</returns>
        public bool Frissit(int id, string nev, string orszagnev, DateTime alapitaseve, int evesforgalom)
        {
            List<Automarka> autok = this.automarkak.Where(x => x.Id == id).Select(x => x).ToList();
            if (autok.Count > 0)
            {
                Automarka auto = autok[0];
                auto.Nev = nev;
                auto.OrszagNev = orszagnev;
                auto.AlapitasEve = alapitaseve;
                auto.EvesForgalom = evesforgalom;
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott márka
        /// int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
        /// </summary>
        /// <param name="id">Automarka ID-je</param>
        /// /// <returns>True sikernél</returns>
        public bool AutomarkaLetezikE(int id)
        {
            return this.All.Any(x => x.Id == id);
        }

        /// <summary>
        /// átlagosmarak/orszag
        /// </summary>
        /// <param name="orszagnev">orsazgnev</param>
        /// <returns>float ertek</returns>
        public virtual double AtlagosMarkaOrszagonkent(string orszagnev)
        {
            orszagnev = orszagnev.ToUpper();
            double markaszama = this.All.Where(x => x.OrszagNev.ToUpper() == orszagnev).Select(x => x).ToList().Count();
            double osszes = this.All.Select(x => x).ToList().Count();
            return markaszama / osszes;
        }
    }
}
