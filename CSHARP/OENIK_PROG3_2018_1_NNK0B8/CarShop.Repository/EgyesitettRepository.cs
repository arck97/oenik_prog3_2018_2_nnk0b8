﻿// <copyright file="EgyesitettRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// a repositorykat egy repositoryba teszük
    /// </summary>
    public class EgyesitettRepository
    {
        /// <summary>
        /// automarka repositorya
        /// </summary>
        private readonly AutomarkaRepository autorepo;

        /// <summary>
        /// extrak repositorya
        /// </summary>
        private readonly ExtraRepository extrarepo;

        /// <summary>
        /// modellek reposytoria
        /// </summary>
        private readonly ModellRepository modelrepo;

        /// <summary>
        /// modellExtrakapcsolo reposytoria
        /// </summary>
        private readonly ModellExtraKapcsoloRepository mekrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EgyesitettRepository"/> class.
        /// </summary>
        public EgyesitettRepository()
        {
            this.autorepo = new AutomarkaRepository();
            this.extrarepo = new ExtraRepository();
            this.modelrepo = new ModellRepository();
            this.mekrepo = new ModellExtraKapcsoloRepository();
        }

        /// <summary>
        /// Gets automarkaRepository
        /// </summary>
        public virtual AutomarkaRepository Autorepo { get => this.autorepo; }

        /// <summary>
        /// Gets ModellExtraKapcsoloRepository
        /// </summary>
        public virtual ModellExtraKapcsoloRepository Mekrepo { get => this.mekrepo; }

        /// <summary>
        /// Gets ModellRepository
        /// </summary>
        public virtual ModellRepository Modelrepo { get => this.modelrepo; }

        /// <summary>
        /// Gets gets ExtraRepository
        /// </summary>
        public virtual ExtraRepository Extrarepo { get => this.extrarepo; }

        /// <summary>
        /// a tabla neveit adja vissza
        /// </summary>
        /// <returns>listaba a tabla nevei</returns>
        public List<string> Tablanevek()
        {
            return AdatbazisElero.Tablanevek;
        }
    }
}
