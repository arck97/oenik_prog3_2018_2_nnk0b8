﻿// <copyright file="ExtraRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// extrak repositorya
    /// </summary>
    public class ExtraRepository : IRepository<Extrak>
    {
        /// <summary>
        /// extrak tablat valositja meg
        /// </summary>
        private readonly List<Extrak> extrak;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraRepository"/> class.
        /// </summary>
        public ExtraRepository()
        {
            this.extrak = AdatbazisElero.Extrak;
        }

        /// <summary>
        /// Gets ezzel tudom lekérni az extrakat
        /// </summary>
        /// <returns>extrakat adja vissza </returns>
        public virtual IQueryable<Extrak> All => this.extrak.AsQueryable<Extrak>();

        /// <summary>
        /// Gets stringet épit
        /// </summary>
        /// <returns>stringbe füzi az adatokat</returns>
        public StringBuilder TablaTartalma
        {
            get
            {
                List<Extrak> temp = this.All.ToList();
                StringBuilder b = new StringBuilder();
                foreach (var item in temp)
                {
                    b.AppendLine("-* ID: " + item.Id + " , NÉV: " + item.Nev + " , KATEGÓRIA: " + item.KategoriaNev + " , SZÍN: " + item.Szin + " , ÁR: " + item.Ar + " , TÖBBSZÖR HASZNÁLHATÓ: " + item.TobszorHasznalhatoE);
                }

                return b;
            }
        }

        /// <summary>
        /// tölri az adott elemet
        /// </summary>
        /// <param name="id">Id-ju elemet kitörli az extrak tablabol </param>
        /// <returns>treu ha sikeres</returns>
        public bool Remove(int id)
        {
            List<Extrak> extra = this.extrak.Where(t => t.Id == id).Select(x => x).ToList();
            if (extra.Count > 0)
            {
                AdatbazisElero.DeleteExtra(extra[0]);
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Egy elem hozzáadása az etrák táblához
        /// </summary>
        /// <param name="kategorianev">extra kategóriájának a neve</param>
        /// <param name="nev"> extra neve</param>
        /// <param name="ar"> extra ára</param>
        /// <param name="szin"> extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">öbbször használható-e az adott extra</param>
        /// <returns>True ha sikeres </returns>
        public bool HozzaAd(string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            Extrak extra = new Extrak()
            {
                KategoriaNev = kategorianev,
                Nev = nev,
                Ar = ar,
                Szin = szin,
                TobszorHasznalhatoE = tobbszor_Hasznalhato_E
            };
            AdatbazisElero.AddExtra(extra);
            AdatbazisElero.SaveChange();
            return true;
        }

        /// <summary>
        /// Egy elem hozzáadása az etrák táblához
        /// </summary>
        /// <param name="id" >frisítendő elem ID-ja</param>
        /// <param name="kategorianev">frissítendő extra kategóriájának neve</param>
        /// <param name="nev">extra neve</param>
        /// <param name="ar">extra ára</param>
        /// <param name="szin">extra színe</param>
        /// <param name="tobbszor_Hasznalhato_E">többször használható-e az extra</param>
        /// <returns>True ha sikeres</returns>
        public bool Frissit(int id, string kategorianev, string nev, int ar, string szin, bool tobbszor_Hasznalhato_E)
        {
            List<Extrak> extra = this.extrak.Where(x => x.Id == id).Select(x => x).ToList();
            if (extra.Count > 0)
            {
                extra[0].KategoriaNev = kategorianev;
                extra[0].Nev = nev;
                extra[0].Ar = ar;
                extra[0].Szin = szin;
                extra[0].TobszorHasznalhatoE = tobbszor_Hasznalhato_E;
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Megvizsgálja létezik-e egy adott extra
        /// </summary>
        /// <param name="id">extra ID-ja</param>
        /// <returns>True ha létezik az extra</returns>
        public bool ExtraletezikE(int id)
        {
            return this.All.Any(x => x.Id == id);
        }

        /// <summary>
        /// Gets the average for the extra color.
        /// </summary>
        /// <param name="color">color.</param>
        /// <returns>float.</returns>
        public virtual double AtlagExtraSzin(string color)
        {
            color = color.ToLower();
            double brandcount = this.All.Where(x => x.Szin.ToLower() == color).Select(x => x).ToList().Count;
            double all = this.All.Select(x => x).ToList().Count;
            return brandcount / all;
        }
    }
}