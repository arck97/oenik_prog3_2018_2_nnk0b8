﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// repositoryk majd ezt kelll hogymegvalositsák
    /// </summary>
    /// <typeparam name="T">repokat</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets stringé konvertálja a tablak tartamat
        /// </summary>
        /// <returns>tabla tartalmat stringbe adja vissza</returns>
        StringBuilder TablaTartalma { get; }

        /// <summary>
        /// Gets mindem Iqueryable-s
        /// </summary>
        /// <returns>databazis linqhaots lesz</returns>
        IQueryable<T> All { get; }

        /// <summary>
        /// iD-ju elem torlese
        /// </summary>
        /// <param name="id">elem id-ja amit törlünk</param>
        /// <returns>true-t add vissza</returns>
        bool Remove(int id);
    }
}
