﻿// <copyright file="ModellExtraKapcsoloRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// ModellExtraKapcsolo tabla reprezentáloja
    /// </summary>
    public class ModellExtraKapcsoloRepository : IRepository<ModellExtraKapcsolo>
    {
        /// <summary>
        /// ModelExtrakapcsolo tabla példanya
        /// </summary>
        private readonly List<ModellExtraKapcsolo> mek;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellExtraKapcsoloRepository"/> class.
        /// </summary>
        public ModellExtraKapcsoloRepository()
        {
            this.mek = AdatbazisElero.ModellekExtraKapcsolo;
        }

        /// <summary>
        /// Gets modellextrakapcsolo tablava alakitasa
        /// </summary>
        /// <returns>modellekextrakapcsolo tabla linq-hozhatoként</returns>
        public virtual IQueryable<ModellExtraKapcsolo> All => this.mek.AsQueryable();

        /// <summary>
        /// Gets stringé épiti a tábla tartalmát
        /// </summary>
        /// <returns>tablat stringbe füzve</returns>
        public StringBuilder TablaTartalma
        {
            get
            {
                List<ModellExtraKapcsolo> temp = this.All.ToList();
                StringBuilder b = new StringBuilder();
                foreach (var item in temp)
                {
                    b.AppendLine("-* ID: " + item.Id + " , MODELLID: " + item.ModellID + " , EXTRAID: " + item.ExtrakID);
                }

                return b;
            }
        }

        /// <summary>
        /// adott elem torlese
        /// </summary>
        /// <param name="id">törlendo elem Id-ja</param>
        /// <returns>true ha sikeres</returns>
        public bool Remove(int id)
        {
            List<ModellExtraKapcsolo> meks = this.mek.Where(x => x.Id == id).Select(x => x).ToList();
            if (meks.Count > 0)
            {
                AdatbazisElero.DeleteExtraKapcsolo(meks[0]);
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// hozzadas a modellextrakapcsolo tablahoz
        /// </summary>
        /// <param name="extraid">extrak kapcsolo Id-ja</param>
        /// <param name="modellid">modell kapcsolo Id-ja</param>
        /// <returns>true ha sikeres</returns>
        public bool HozzaAd(int extraid, int modellid)
        {
            ModellExtraKapcsolo meks = new ModellExtraKapcsolo()
            {
                ExtrakID = extraid,
                ModellID = modellid
            };
            AdatbazisElero.AddExtraKapcsolo(meks);
            AdatbazisElero.SaveChange();
            return true;
        }

        /// <summary>
        /// frisite a modellextrakapcsolo tablat
        /// </summary>
        /// <param name="id">uj id</param>
        /// <param name="extraid">extra kapcsolo ID-ja</param>
        /// <param name="modellid">model kapcsolo Id-ja</param>
        /// <returns>true ha igaz</returns>
        public bool Frissit(int id, int extraid, int modellid)
        {
            List<ModellExtraKapcsolo> meks = this.mek.Where(x => x.Id == id).Select(x => x).ToList();
            if (meks.Count() > 0)
            {
                meks[0].ExtrakID = extraid;
                meks[0].ModellID = modellid;
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }
    }
}
