﻿// <copyright file="ModellRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// modell repository
    /// </summary>
    public class ModellRepository : IRepository<Modellek>
    {
        /// <summary>
        /// modell tabla implementacioja
        /// </summary>
        private readonly List<Modellek> modellek;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellRepository"/> class.
        /// </summary>
        public ModellRepository()
        {
            this.modellek = AdatbazisElero.Modellek;
        }

        /// <summary>
        /// Gets modellek tablat adja vissza
        /// </summary>
        /// <returns>Modelleket adja vissza</returns>
        public virtual IQueryable<Modellek> All => this.modellek.AsQueryable();

        /// <summary>
        /// Gets visszaadja stringként a modell táblát
        /// </summary>
        /// <returns>Stringe füzi a modellek tablat</returns>
        public StringBuilder TablaTartalma
        {
            get
            {
                List<Modellek> model = this.All.ToList();
                StringBuilder b = new StringBuilder();
                foreach (var item in model)
                {
                    b.AppendLine("-* ID: " + item.Id + " , NÉV: " + item.Nev + " , MÁRKA_ID: " + item.MarkaID + " , LÓERŐ: " + item.Loero + " , ÁR: " + item.AlapAr + " , MOTOR_TÉRFOGAT: " + item.MotorTerfogat + " ,  MEGJELENÉS: " + item.MegjelenesNapja);
                }

                return b;
            }
        }

        /// <summary>
        /// elem torlese a modellek tablabol
        /// </summary>
        /// <param name="id">törlendo elem ID-ja</param>
        /// <returns>treu ha sikeres</returns>
        public bool Remove(int id)
        {
            List<Modellek> modell = this.modellek.Where(x => x.Id == id).Select(x => x).ToList();
            if (modell.Count > 0)
            {
                AdatbazisElero.DeleteModell(modell[0]);
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// hozzadad egy uj elemet a model tablahoz
        /// </summary>
        /// <param name="markaid">hozadando elem marka Id-ja</param>
        /// <param name="megjelenes">megjelenesiideje</param>
        /// <param name="mototerfogat">motorterfogata</param>
        /// <param name="loero">loereje</param>
        /// <param name="alapar">uj elem alapara</param>
        /// <param name="nev">uj elem neve</param>
        /// <returns>treu ha sikres</returns>
        public bool HozzaAd(int markaid, DateTime megjelenes, int mototerfogat, int loero, int alapar, string nev)
        {
            Modellek modell = new Modellek()
            {
                MarkaID = markaid,
                MegjelenesNapja = megjelenes,
                MotorTerfogat = mototerfogat,
                Loero = loero,
                AlapAr = alapar,
                Nev = nev
            };
            AdatbazisElero.AddMarka(modell);
            return true;
        }

        /// <summary>
        /// frisitei a model tablat
        /// </summary>
        /// <param name="id">frisitendo elem Id-ja</param>
        /// <param name="markaid">frisitendo elem neve</param>
        /// <param name="megjelenesiido">frisitendo elem megjelenesenek napja</param>
        /// <param name="mototerfogat">motorterfogat frisitese</param>
        /// <param name="loero">frisitendo elem loereje</param>
        /// <param name="alapar">alapar</param>
        /// <param name="nev">neve</param>
        /// <returns>treu ha sikres</returns>
        public bool Frissit(int id, int markaid, DateTime megjelenesiido, int mototerfogat, int loero, int alapar, string nev)
        {
            List<Modellek> model = this.modellek.Where(x => x.Id == id).Select(x => x).ToList();
            if (model.Count > 0)
            {
                model[0].MarkaID = markaid;
                model[0].MegjelenesNapja = megjelenesiido;
                model[0].MotorTerfogat = mototerfogat;
                model[0].Loero = loero;
                model[0].AlapAr = alapar;
                model[0].Nev = nev;
                AdatbazisElero.SaveChange();
                return true;
            }

            return false;
        }

        /// <summary>
        /// megnézi hogy létezik-e adott Id-ju elem a model tablaban
        /// </summary>
        /// <param name="id">Id-ju elem létének vizsgalata</param>
        /// <returns>true ha igaz</returns>
        public bool ModelletezikE(int id)
        {
            return this.All.Any(x => x.Id == id);
        }

        /// <summary>
        /// Returns the average amount for brands.
        /// </summary>
        /// <param name="brandId">brandid.</param>
        /// <returns>True/False.</returns>
        public float AtlagosModel(int brandId)
        {
            float modelszam = this.All.Where(x => x.MarkaID == brandId).Select(x2 => x2).ToList().Count;
            float all = this.All.Select(x3 => x3).ToList().Count;
            return modelszam / all;
        }
    }
}