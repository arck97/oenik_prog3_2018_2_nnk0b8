﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarShop.Data;


namespace CarShop.Repository
{
    public interface IRepository<T>
    {
        void Hozzaad(string sqlparancs);
        void Torles(string sqlparancs);
        void Frissit(string sqlparancs);
        List<T> Olvas();

        

    }
}

   
