﻿// <copyright file="JavaWebShop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2018_1_NNK0B8
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// javaosztaly Java végpont megvalosításhoz
    /// </summary>
   public class JavaWebShop
   {
        /// <summary>
        /// Gets or sets  alapítasdatuma
        /// </summary>
        public string ALAPITASDATUM
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  alapítasdatuma
        /// </summary>
        public string MOTORTERFOGAT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  alapítasdatuma
        /// </summary>
        public string LOERO
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  alapítasdatuma
        /// </summary>
        public string ALAPAR
        {
            get;
            set;
        }
    }
}
