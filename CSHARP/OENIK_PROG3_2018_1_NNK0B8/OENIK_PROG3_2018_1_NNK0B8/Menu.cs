﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2018_1_NNK0B8
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;
    using ConsoleApp1;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// menu osztaly
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// BuseinessLogic repressentation
        /// </summary>
        private readonly BusinessLogic bl = new BusinessLogic();

        /// <summary>
        /// Gets the index of the first starter tag and the length of it
        /// And the index of the end tag, than substrings.
        /// </summary>
        /// <param name="s">String input.</param>
        /// <param name="tag">Looking for this tag in s.</param>
        /// <returns>Returns data between tags.</returns>
        internal string FullResponseTrim(string s, string tag)
        {
            string kezdoTag = "<" + tag + ">";
            int startIndex = s.IndexOf(kezdoTag, StringComparison.Ordinal) + kezdoTag.Length;
            int endIndex = s.IndexOf("</" + tag + ">", startIndex, StringComparison.Ordinal);
            return s.Substring(startIndex, endIndex - startIndex);
        }

        /// <summary>
        /// menu elvegzi a feladatot
        /// </summary>
        /// <param name="menupont">menupont valasztas</param>
        /// <returns>False ha ki akarunk lépni</returns>
        internal bool MuveletVegzes(int menupont)
        {
            switch (menupont)
            {
                case 11:
                    try
                    {
                        // int ID,string Nev,string OrszagNev, DateTime AlapitasEve, int EvesForgalom
                        Console.WriteLine("Elem hozzáadása az autómárka táblához.");
                        Console.WriteLine("Adja meg az autómárkát: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az országot: ");
                        string orszagnev = Console.ReadLine();
                        Console.WriteLine("Adja meg az alapítási évet: (yyyy-MM-dd)");
                        DateTime alapitasiEv = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az éves forgalmat");
                        int evesForgalom = int.Parse(Console.ReadLine());
                        this.bl.HozzaadAutomarka(nev, orszagnev, alapitasiEv, evesForgalom);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 12:
                    try
                    {
                        // int ID, int marka_id, DateTime megjelenes_napja, int motorterfogat, int loero, int alapar, string nev
                        Console.WriteLine("Elem hozzáadása a modellek táblához.");
                        Console.WriteLine("Adja meg a modell márkáját: ");
                        string marka = Console.ReadLine();
                        int marka_id = this.bl.AutomarkaKereses(marka);
                        Console.WriteLine("Adja meg a modell nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg a megjelenés napját: ");
                        DateTime megjelnes_napja = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a motor térfogatot:");
                        int motorterfogat = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a lóerőt: ");
                        int loero = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az alapárat: ");
                        int alapar = int.Parse(Console.ReadLine());
                        this.bl.HozzaadModellek(marka_id, megjelnes_napja, motorterfogat, loero, alapar, nev);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 13:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem hozzáadása a extrák táblához.");
                        Console.WriteLine("Adja meg az extra felszerelés kategóriáját ");
                        string kategorianev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra felszerelés nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra felszerelés árát: ");
                        int ar = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az extra felszerelés színét: ");
                        string szin = Console.ReadLine();
                        Console.WriteLine("Többször használható az extra felszerelés?(igen/nem) ");
                        string the = Console.ReadLine();
                        bool tobbszor_hasznalhato_e = the == "igen" ? true : false;
                        this.bl.HozzaadExtrak(kategorianev, nev, ar, szin, tobbszor_hasznalhato_e);
                        Console.WriteLine("Sikeres hozzáadás!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen hozzáadás! " + e);
                    }

                    break;
                case 21:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az autómárka táblából ");
                        Console.WriteLine("Adja meg a törlenddő automárka nevét");
                        string automarka = Console.ReadLine();
                        this.bl.RemoveAutomarka(this.bl.AutomarkaKereses(automarka));
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 22:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az modellek táblából ");
                        Console.WriteLine("Adja meg a törlenddő modell ID-jét");
                        int modell_id = int.Parse(Console.ReadLine());
                        this.bl.RemoveModellek(modell_id);
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 23:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Elem törlése az extrák táblából ");
                        Console.WriteLine("Adja meg a törlenddő extra ID-jét");
                        int extra_id = int.Parse(Console.ReadLine());
                        this.bl.RemoveExtra(extra_id);
                        Console.WriteLine("Sikeres törlés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 31:
                    try
                    {
                        Console.WriteLine("Elem frissítése az autómárka táblában ");
                        Console.WriteLine("Adja meg a frissítendő automárka nevét");
                        string automarka = Console.ReadLine();
                        int id = this.bl.AutomarkaKereses(automarka);
                        Console.WriteLine("Adja meg az országot: ");
                        string orszagnev = Console.ReadLine();
                        Console.WriteLine("Adja meg az alapítási évet: (yyyy-MM-dd)");
                        DateTime alapitasiEv = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az éves forgalmat");
                        int evesForgalom = int.Parse(Console.ReadLine());
                        this.bl.FirissitAutomarka(id, automarka, orszagnev, alapitasiEv, evesForgalom);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 32:
                    try
                    {
                        Console.WriteLine("Elem frissítése a modellek táblában ");
                        Console.WriteLine("Adja meg a frissítendő modell ID-jét");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a márkát: ");
                        string marka = Console.ReadLine();
                        int marka_id = this.bl.AutomarkaKereses(marka);
                        Console.WriteLine("Adja meg a modell nevét: ");
                        string modell_nev = Console.ReadLine();
                        Console.WriteLine("Adja meg a megjelenés napját: (yyyy-MM-dd)");
                        DateTime megjelenes_napja = DateTime.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a motortérfogatot");
                        int motorterfogat = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a lóerőt");
                        int loero = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az alapárat");
                        int alapar = int.Parse(Console.ReadLine());
                        this.bl.FirissitModellek(id, marka_id, megjelenes_napja, motorterfogat, loero, alapar, modell_nev);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;
                case 33:
                    try
                    {
                        Console.WriteLine("Elem frissítése az extrák táblában ");
                        Console.WriteLine("Adja meg a frissítendő extra ID-jét");
                        int id = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg a kategóriát: ");
                        string kategorianev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra nevét: ");
                        string nev = Console.ReadLine();
                        Console.WriteLine("Adja meg az extra árát: ");
                        int ar = int.Parse(Console.ReadLine());
                        Console.WriteLine("Adja meg az extra színét: ");
                        string szin = Console.ReadLine();
                        Console.WriteLine("Többször használható az extra felszerelés?(igen/nem) ");
                        string the = Console.ReadLine();
                        bool tobbszor_hasznalhato_e = the == "igen" ? true : false;
                        this.bl.FirissitExtra(id, kategorianev, nev, ar, szin, tobbszor_hasznalhato_e);
                        Console.WriteLine("Sikeres frissítés!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen törlés! " + e);
                    }

                    break;

                case 41:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Autómárka tábla tartalma: ");
                        Console.WriteLine(this.bl.AutomarkaTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 42:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Modellek tábla tartalma: ");
                        Console.WriteLine(this.bl.ModellTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 43:
                    try
                    {
                        // int ID, string kategorianev, string nev, int ar, string szin, bool tobbszor_hasznalhato_e
                        Console.WriteLine("Extrák tábla tartalma: ");
                        Console.WriteLine(this.bl.ExtraTartalom);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sikertelen listázás! " + e);
                    }

                    break;
                case 51:
                    Console.WriteLine("Az  2000 utáni :");
                    foreach (var item in this.bl.CarAfter2000())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 52:
                    Console.WriteLine("Az alapár magasabb mint a bevetel :");
                    foreach (var item in this.bl.AlaparMagasabbMintEvesForgalom())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 53:
                    Console.WriteLine("USA márkák :");
                    foreach (var item in this.bl.AmerikaiAutokModell())
                    {
                        Console.WriteLine(item);
                    }

                    break;
                case 54:
                    Console.WriteLine("Adja meg melyik ORSZÁG márkája átlagára kiváncsi");
                    string kivantmarka = Console.ReadLine();
                    double avg = this.bl.GetAtlagMarkak(kivantmarka);
                    Console.WriteLine("Átlagos darabszám: " + avg);
                    break;
                case 55:
                    Console.WriteLine("Adja meg a milyen extra SZIN átlagára kiváncsi");
                    string szin1 = Console.ReadLine();
                    double avg1 = this.bl.GetAtlagExtraSzin(szin1);
                    Console.WriteLine("Átlagos darabszám: " + avg1);
                    break;
                case 56:
                    Console.WriteLine("Adja meg a márka ID-ját ha kiváncsi az adott marka modell átlagára:");
                    int id1 = int.Parse(Console.ReadLine());
                    double avg2 = this.bl.GetAtlagModel(id1);
                    Console.WriteLine("Átlagos darabszám: " + avg2);
                    break;
                  default:
                    return false;
            }

            Console.ReadLine();
            return false;
        }

        /// <summary>
        /// Kiírja a menusort
        /// </summary>
        /// <returns>menupont száma</returns>
        internal int Menusor()
        {
            int menupont;
            int almenupont;
            int valasztas = 0;
            bool fomenu = true;
            bool almenu = true;
            while (fomenu)
            {
                Console.WriteLine("1 - Elem hozzáadása megadott táblához");
                Console.WriteLine("2 - Elem törlése megadott táblából");
                Console.WriteLine("3 - Elem frissítése megadott táblából");
                Console.WriteLine("4 - Elemek listázása megadott táblából");
                Console.WriteLine("5 - egyéb műveletek - átlag");
                Console.WriteLine("6 - Random érték és mentés");
                if (int.TryParse(Console.ReadLine(), out menupont))
                {
                    switch (menupont)
                    {
                        case 1:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblához szeretne új elemet hozzáadni?");
                                Console.WriteLine("1 - Automárkák");
                                Console.WriteLine("2 - Modellek");
                                Console.WriteLine("3 - Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 11;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 12;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 13;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 2:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblából szeretne törölni?");
                                Console.WriteLine("1 - Automárkák");
                                Console.WriteLine("2 - Modellek");
                                Console.WriteLine("3 - Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 21;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 22;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 23;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 3:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblában szeretne frissíteni?");
                                Console.WriteLine("1 - Automárkák");
                                Console.WriteLine("2 - Modellek");
                                Console.WriteLine("3 - Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 31;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 32;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 33;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 4:
                            while (almenu)
                            {
                                Console.WriteLine("Melyik táblát szeretné kilistázni?");
                                Console.WriteLine("1 - Automárkák");
                                Console.WriteLine("2 - Modellek");
                                Console.WriteLine("3 - Extrák");
                                if (int.TryParse(Console.ReadLine(), out almenupont))
                                {
                                    switch (almenupont)
                                    {
                                        case 1:
                                            valasztas = 41;
                                            almenu = false;
                                            break;
                                        case 2:
                                            valasztas = 42;
                                            almenu = false;
                                            break;
                                        case 3:
                                            valasztas = 43;
                                            almenu = false;
                                            break;
                                        default:
                                            Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                            Console.ReadLine();
                                            break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Érvénytlen! Kérem válasszon újra!");
                                    Console.ReadLine();
                                }
                            }

                            fomenu = false;
                            break;
                        case 5:
                            Console.WriteLine("1 - 2000 utáni modellek");
                            Console.WriteLine("2 - Alapár magasabb mint a bevetel");
                            Console.WriteLine("3 - USA automárák és modelljeik");
                            Console.WriteLine("4 - Márkák átlaga");
                            Console.WriteLine("5 - Extra szinek átlaga");
                            Console.WriteLine("6 - Modellek átlaga");
                            if (int.TryParse(Console.ReadLine(), out almenupont))
                            {
                                switch (almenupont)
                                {
                                    case 1:
                                        valasztas = 51;
                                        almenu = false;
                                        break;
                                    case 2:
                                        valasztas = 52;
                                        almenu = false;
                                        break;
                                    case 3:
                                        valasztas = 53;
                                        almenu = false;
                                        break;
                                    case 4:
                                        valasztas = 54;
                                        almenu = false;
                                        break;
                                    case 5:
                                        valasztas = 55;
                                        almenu = false;
                                        break;
                                    case 6:
                                        valasztas = 56;
                                        almenu = false;
                                        break;
                                    default:
                                        Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                        Console.ReadLine();
                                        break;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                                Console.ReadLine();
                            }

                            fomenu = false;
                            break;
                        case 6:
                            try
                            {
                                WebClient client = new WebClient();
                                Console.WriteLine("Rekord modostitás nem lehetséges!");
                                Console.WriteLine("ADATBEKÉRÉS UTÁN LÉTEZŐ ID MEGADÁSAKOR HIBÁT KAP!");

                                Console.WriteLine("Adjon meg egy minimális árat!");
                                string armin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális árat!");
                                string armax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális évet!");
                                string evmin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális évet!");
                                string evmax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális motortérfogatot!");
                                string motormin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális motortérfogatot!");
                                string motormax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális lóerő!");
                                string loeromin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális lóerő!");
                                string loeromax = Console.ReadLine();

                                client.Encoding = Encoding.UTF8;
                                string response = client.DownloadString(Start.LocationURL + "?release=" + evmin + "-" + evmax + "&engine=" + motormin + "-" + motormax + "&hp=" + loeromin + "-" + loeromax + "&price=" + armin + "-" + armax);

                                string fullresponse = this.FullResponseTrim(response, "body").Trim();
                                if (fullresponse.Contains("Hiba!"))
                                {
                                    Console.WriteLine("Lekérdezési hibák: " + fullresponse);
                                }
                                else
                                {
                                    Console.WriteLine("válasz: " + fullresponse);
                                    JavaWebShop modell = JsonConvert.DeserializeObject<JavaWebShop>(fullresponse);

                                    int datum = int.Parse(modell.ALAPITASDATUM);
                                    int motor = int.Parse(modell.MOTORTERFOGAT);
                                    int loero = int.Parse(modell.LOERO);
                                    int ar = int.Parse(modell.ALAPAR);

                                BrandDoesntExist:
                                    Console.WriteLine("Adja meg a márka ID-jét!");
                                    string id = Console.ReadLine();
                                    bool b = int.TryParse(id, out int brandid);
                                    while (!b)
                                    {
                                        Console.WriteLine("Adja meg a márka ID-jét!");
                                        id = Console.ReadLine();
                                        b = int.TryParse(id, out brandid);
                                    }

                                    if (!new BusinessLogic().LetezikAutomarka(brandid))
                                    {
                                        Console.WriteLine("Nem létező az ID!");
                                        goto BrandDoesntExist;
                                    }

                                    Console.WriteLine("Adjon meg egy modell nevet és  rekord elmentödik!");
                                    string modellname = Console.ReadLine();

                                    bool success = new BusinessLogic().HozzaadModellek(brandid, new DateTime(datum, 1, 10), motor, loero, ar, modellname);
                                    string message = success ? "A bevitel a szerverről sikerült." : "A bevitel a szerverről nem sikerült.";
                                    Console.WriteLine(message);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("hibába ütköztünk: " + e.Message);
                                Console.WriteLine();
                            }

                            break;

                        default:
                            Console.WriteLine("Érvénytlen menüpont! Kérem válasszon újra!");
                            Console.ReadLine();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Ki szeretne lépni? (igen/nem)");
                    string valasz = Console.ReadLine();
                    if (valasz == "igen")
                    {
                        fomenu = false;
                        valasztas = 0;
                    }
                    else
                    {
                        Console.WriteLine("Folytatjuk, üssön egy entert");
                        Console.ReadLine();
                        Console.Clear();
                    }
                }
            }

            return valasztas;
        }
    }
}