﻿// <copyright file="Start.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ConsoleApp1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OENIK_PROG3_2018_1_NNK0B8;

    /// <summary>
    /// main program menu futattasa
    /// </summary>
    public static class Start
    {
        /// <summary>
        /// Location URL Java végpont.
        /// </summary>
        public const string LocationURL = "http://localhost:8080/JavaWeb/";

        /// <summary>
        /// main menu
        /// </summary>
        public static void Main()
        {
            bool elindul = true;

            while (elindul)
            {
                Menu m = new Menu();
               m.MuveletVegzes(m.Menusor());
            }
        }
    }
}
