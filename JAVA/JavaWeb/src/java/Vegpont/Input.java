/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vegpont;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Peti(ke)
 */
@WebServlet(name = "Input", urlPatterns = {"/Input"})

public class Input extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        PrintWriter out = response.getWriter();
        try {
            String alapitasdatum = request.getParameter("alapitasdatum");
            if (!alapitasdatum.contains("-")) {
                out.println("SYNTAX alapitasdatuma: X-Y");
                return;
            }
            String motorterfogat = request.getParameter("motorterfogat");
            if (!motorterfogat.contains("-")) {
                out.println("SYNTAX motorterfogat: X-Y");
                return;
            }
            String loero = request.getParameter("loero");
            if (!loero.contains("-")) {
                out.println("SYNTAX loero: X-Y");
                return;
            }
            String alapar = request.getParameter("alapar");
            if (!alapar.contains("-")) {
                out.println("SYNTAX alapar: X-Y");
                return;
            }
            out.println(VegpontGeneralas.getJson(alapitasdatum, motorterfogat, loero, alapar));
        }
        catch (Exception e) {
            out.println("Hiba! Valószínűleg hibás a parameter átadás " + e);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Rövid leiras";
    }
}
