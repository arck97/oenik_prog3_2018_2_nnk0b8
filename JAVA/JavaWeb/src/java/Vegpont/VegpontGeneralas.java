/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vegpont;


import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author klric
 */

/**
 *
 * @author Peti(ke)
 */
public class VegpontGeneralas 
{
     private static final Random r = new Random();
     
     private static int GenerateRandom(String param) 
    {
        String[] spl = param.split("-");
        int min = Integer.parseInt(spl[0]);
        int max = Integer.parseInt(spl[1]);
        return r.nextInt((max - min) + 1) + min;
    }
     
     public static String getJson(String alapitasdatum, String motorterfogat,
             String loero, String alapar) 
    {
        JsonObjectBuilder jobj = Json.createObjectBuilder();
        jobj.add("ALAPITASDATUM", GenerateRandom(alapitasdatum));
        jobj.add("MOTORTERFOGAT", GenerateRandom(motorterfogat));
        jobj.add("LOERO", GenerateRandom(loero));
        jobj.add("ALAPAR", GenerateRandom(alapar));
        JsonObject complete = jobj.build();
        return complete.toString();
    }
}