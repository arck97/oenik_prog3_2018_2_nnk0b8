<%-- 
    Document   : index
    Created on : 2018.12.21., 0:24:55
    Author     : Peti(ke)
--%>

<%@page import="Vegpont.VegpontGeneralas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NNK0B8 beadando JSP oldal</title>   
        <body>
        <%
            if (request.getParameter("alapitasdatum") == null
                || request.getParameter("motorterfogat") == null
                || request.getParameter("loero") == null
                || request.getParameter("alapar") == null) {
                    out.println("Hibás paraméterek!");
            } else {
                ServletContext context = getServletContext();
                RequestDispatcher rd = context.getRequestDispatcher("/Input");
                rd.forward(request, response);
            }
        %>
        </body>
    </head>
</html>
