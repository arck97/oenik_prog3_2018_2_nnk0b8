var class_car_shop_1_1_data_1_1_adatbazis_elero =
[
    [ "AddAutomarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a52e7af1d37b2f87a695257002e4743ff", null ],
    [ "AddExtra", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#ae378c771962c2344f2df4c1a1f67fda3", null ],
    [ "AddExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a38c7dbaaa8a977a879708fde10eeeafd", null ],
    [ "AddMarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a982ba177f9888999a453e2d3664ee191", null ],
    [ "DeleteAutomarka", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a29a6b26262466ed6f5749958f1c33914", null ],
    [ "DeleteExtra", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#ab76c6d2764619c5a611b2555b1bdd679", null ],
    [ "DeleteExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a624f42766aaf67c540f6ea39bee9791f", null ],
    [ "DeleteModell", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a5efad99ae71c0e1826c4b966d223e37b", null ],
    [ "SaveChange", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#aad16f6630c90e8ab87df0530239ced2d", null ],
    [ "Automarkak", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a8997002e181d6cb15d2b4d3f2b9395c2", null ],
    [ "CarshopAllomany", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a4fa750e2534c9d9c1d4d7528fbdaec56", null ],
    [ "Extrak", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a15bc686f635c250a1cf527a35425861a", null ],
    [ "Modellek", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#acfd702cc3a7a16ae4dad958e5d3163bc", null ],
    [ "ModellekExtraKapcsolo", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#a6e9af348f31aa4561a9745470a01e0b4", null ],
    [ "Tablanevek", "class_car_shop_1_1_data_1_1_adatbazis_elero.html#acc1cd04002baa4afc0be4ca9c7d8ba16", null ]
];