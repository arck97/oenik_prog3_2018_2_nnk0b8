var hierarchy =
[
    [ "CarShop.Data.AdatbazisElero", "class_car_shop_1_1_data_1_1_adatbazis_elero.html", null ],
    [ "CarShop.Data.Automarka", "class_car_shop_1_1_data_1_1_automarka.html", null ],
    [ "CarShop.Logic.Tests.Class1", "class_car_shop_1_1_logic_1_1_tests_1_1_class1.html", null ],
    [ "CarShop.Repository.Tests.Class1", "class_car_shop_1_1_repository_1_1_tests_1_1_class1.html", null ],
    [ "CarShop.Data.DateBaseKezelo", "class_car_shop_1_1_data_1_1_date_base_kezelo.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.CarShopDatabasedEntities", "class_car_shop_1_1_data_1_1_car_shop_databased_entities.html", null ]
    ] ],
    [ "CarShop.Repository.EgyesitettRepository", "class_car_shop_1_1_repository_1_1_egyesitett_repository.html", null ],
    [ "CarShop.Data.Extrak", "class_car_shop_1_1_data_1_1_extrak.html", null ],
    [ "CarShop.Logic.ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.BusinessLogic", "class_car_shop_1_1_logic_1_1_business_logic.html", null ],
      [ "CarShop.Logic.Logic", "class_car_shop_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< Automarka >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.AutomarkaRepository", "class_car_shop_1_1_repository_1_1_automarka_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< Extrak >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtraRepository", "class_car_shop_1_1_repository_1_1_extra_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< Modellek >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellRepository", "class_car_shop_1_1_repository_1_1_modell_repository.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< ModellExtraKapcsolo >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellExtraKapcsoloRepository", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository.html", null ]
    ] ],
    [ "OENIK_PROG3_2018_1_NNK0B8.JavaWebShop", "class_o_e_n_i_k___p_r_o_g3__2018__1___n_n_k0_b8_1_1_java_web_shop.html", null ],
    [ "CarShop.Logic.Tests.LogicTests", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "OENIK_PROG3_2018_1_NNK0B8.Menu", "class_o_e_n_i_k___p_r_o_g3__2018__1___n_n_k0_b8_1_1_menu.html", null ],
    [ "CarShop.Data.Modellek", "class_car_shop_1_1_data_1_1_modellek.html", null ],
    [ "CarShop.Data.ModellExtraKapcsolo", "class_car_shop_1_1_data_1_1_modell_extra_kapcsolo.html", null ],
    [ "ConsoleApp1.Program", "class_console_app1_1_1_program.html", null ],
    [ "CarShop.Repository.Reposegitseg", "class_car_shop_1_1_repository_1_1_reposegitseg.html", null ],
    [ "CarShop.Repository.Tests.RepoTest", "class_car_shop_1_1_repository_1_1_tests_1_1_repo_test.html", null ],
    [ "ConsoleApp1.Start", "class_console_app1_1_1_start.html", null ],
    [ "Vegpont.VegpontGeneralas", "class_vegpont_1_1_vegpont_generalas.html", null ],
    [ "HttpServlet", null, [
      [ "Vegpont.Input", "class_vegpont_1_1_input.html", null ]
    ] ]
];