var interface_car_shop_1_1_logic_1_1_i_logic =
[
    [ "FirissitAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a933ec69804c6a1e37fea9ef91d64426e", null ],
    [ "FirissitExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a6fd79bb1aac1be2e848c98cdb423a7b0", null ],
    [ "FirissitModellek", "interface_car_shop_1_1_logic_1_1_i_logic.html#a3fc3ac681dd578f36575bee187dc3008", null ],
    [ "FirssitKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#a03fe74ee66b59a8f20ebc378f71c8d65", null ],
    [ "HozzaadAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a22cc23369f844590ad1d53b9e706a814", null ],
    [ "HozzaadExtrak", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab9b9b316df37119d03150712b291bd6f", null ],
    [ "HozzaadModellek", "interface_car_shop_1_1_logic_1_1_i_logic.html#a68a7e5dd3d52f11c747d5fdf69efda0d", null ],
    [ "HozzadKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#a44006974a5e9c2ddf1ecedf1d6108db4", null ],
    [ "LetezikAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a7d0d728027629e361fe1bf70de641bdc", null ],
    [ "LetezikExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a67801ccaf5f729fc364772d958e50513", null ],
    [ "LetezikModell", "interface_car_shop_1_1_logic_1_1_i_logic.html#af75c92846655a24c89a9c9c5a69d44ea", null ],
    [ "RemoveAutomarka", "interface_car_shop_1_1_logic_1_1_i_logic.html#a18eff06f4d27dd6c94cbc5e9f8ecc06c", null ],
    [ "RemoveExtra", "interface_car_shop_1_1_logic_1_1_i_logic.html#a11903f7c4083593f82ae2f25f0f180b7", null ],
    [ "RemoveKapcsolo", "interface_car_shop_1_1_logic_1_1_i_logic.html#a8adb84437a38a63d28146156f77a524b", null ],
    [ "RemoveModellek", "interface_car_shop_1_1_logic_1_1_i_logic.html#a97c2b4c17906c9b4897616ae291b3d2c", null ],
    [ "AutomarkaTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a59ea9878ab0216474a551cce804f0870", null ],
    [ "ExtraTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5fa88cff49459ab54282c653c070e859", null ],
    [ "Kapcsolotartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a3e03e3f2b36e72642fb13ff9dda52102", null ],
    [ "ModellTartalom", "interface_car_shop_1_1_logic_1_1_i_logic.html#a0b242ee792038ad0348b082f2065bc0f", null ],
    [ "OsszesTabla", "interface_car_shop_1_1_logic_1_1_i_logic.html#ad9b2a961cca081e6c979b5826e47294b", null ]
];