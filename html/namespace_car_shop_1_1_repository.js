var namespace_car_shop_1_1_repository =
[
    [ "Tests", "namespace_car_shop_1_1_repository_1_1_tests.html", "namespace_car_shop_1_1_repository_1_1_tests" ],
    [ "AutomarkaRepository", "class_car_shop_1_1_repository_1_1_automarka_repository.html", "class_car_shop_1_1_repository_1_1_automarka_repository" ],
    [ "EgyesitettRepository", "class_car_shop_1_1_repository_1_1_egyesitett_repository.html", "class_car_shop_1_1_repository_1_1_egyesitett_repository" ],
    [ "ExtraRepository", "class_car_shop_1_1_repository_1_1_extra_repository.html", "class_car_shop_1_1_repository_1_1_extra_repository" ],
    [ "IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "ModellExtraKapcsoloRepository", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository.html", "class_car_shop_1_1_repository_1_1_modell_extra_kapcsolo_repository" ],
    [ "ModellRepository", "class_car_shop_1_1_repository_1_1_modell_repository.html", "class_car_shop_1_1_repository_1_1_modell_repository" ],
    [ "Reposegitseg", "class_car_shop_1_1_repository_1_1_reposegitseg.html", null ]
];